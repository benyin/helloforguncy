# build code 
FROM mcr.microsoft.com/dotnet/core/sdk:latest
WORKDIR /helloforguncy

#maintainer info
MAINTAINER benyin

COPY . ./

# copy csproj and restore as distinct layers
RUN dotnet publish -c Release -o out



